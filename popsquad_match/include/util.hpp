#include <vector>
#include <string>
#include <iostream>

namespace util
{
    void split_string(std::string s, std::string delim, std::vector<std::string>& resp)
    {
        auto start = 0U;
        auto end = s.find(delim);
        while (end != std::string::npos)
        {
            resp.push_back(s.substr(start, end));
            start = end + delim.length();
            end = s.find(delim, start);
        }

        resp.push_back(s.substr(start, end));
    }
}
