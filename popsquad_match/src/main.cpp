#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <vector>
#include <sstream>
#include <curl/curl.h>
#include <thread>
#include <string>

#include "util.hpp"
#include "sole.hpp"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include <sparsepp/spp.h>

#define BUFLEN 256
#define PORT 9490

using namespace rapidjson;

int ssock;

struct MemoryStruct {
    char *memory;

    size_t size;
};

struct Connection
{
public:
    char pack_state;

    std::stringstream curr_packs;

    std::string id;
    std::string prev_packs;
    std::string party_id;
    Connection()
    {
        party_id = "";
    }
};

struct Party
{
public:
    std::string party_name;
    std::vector<std::string>* members;

    Party()
    {
        party_name = "New Party";
        members = new std::vector<std::string>(4);
    }
};

spp::sparse_hash_map<std::string, Connection*> con_list;
spp::sparse_hash_map<char*, std::string> ip_to_id;
spp::sparse_hash_map<std::string, Party*> parties;

std::vector<std::string> queue;

static size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *) userp;

    mem->memory = (char*) realloc(mem->memory, mem->size + realsize + 1);
    if(mem->memory == NULL) {
        /* out of memory! */
        printf("not enough memory (realloc returned NULL)\n");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

void crash(const char* ssock)
{
    perror(ssock);
    exit(1);
}

void flip(Connection* con)
{
    if(con->pack_state == 'x') con->pack_state = 'y';
    else con->pack_state = 'x';
}

bool check_auth(char* buf, sockaddr_in* client)
{
    CURL *curl_handle;
    CURLcode res;

    struct MemoryStruct chunk;

    chunk.memory = (char*) malloc(1);  /* will be grown as needed by the realloc above */
    chunk.size = 0;    /* no data at this point */

    curl_handle = curl_easy_init();

    // get user id from packet
    char iid[36];
    memcpy(iid, &buf[1], 36);
    iid[36] = '\0';
    std::string id(iid);

    std::string url = std::string("http://127.0.0.1:5984/psq_user/");
    url += id;
    url += "/";
    curl_easy_setopt(curl_handle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    res = curl_easy_perform(curl_handle);
    if(res != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
    }
    else
    {
        Document d;
        d.Parse(chunk.memory);
        if(d.HasMember("auth"))
        {
            Value& auth_token = d["auth"];

            // get auth token from packet
            char sent_token[36];
            memcpy(sent_token, &buf[37], 36);
            sent_token[36] = '\0';
            if(strcmp(sent_token, auth_token.GetString()) == 0)
            {
                Connection* con = new Connection();
                con->id = id;
                con->pack_state = 'x';
                con->curr_packs << con->pack_state;

                // protocol notation: rpa = Reply | Auth | Accepted (: end of pack)
                con->curr_packs << ":raa";

                // get ip
                auto ip = inet_ntoa(client->sin_addr);

                // create or update user data
                con_list[id] = con;
                ip_to_id[ip] = id;
            }
        }
    }

    curl_easy_cleanup(curl_handle);
    free(chunk.memory);
    return true;
}

bool is_set(sockaddr_in* client)
{
    return (ip_to_id.find(inet_ntoa(client->sin_addr)) != ip_to_id.end());
}

void process(char* buf, sockaddr_in* client, socklen_t* slen)
{
    auto id = ip_to_id.at(inet_ntoa(client->sin_addr));
    auto con = con_list.at(id);

    // parse packet
    std::string packet(buf);
    std::vector<std::string> arg;
    util::split_string(packet, "~", arg);
    std::string cmd = arg.at(0);

    // init Party
    if (cmd == "pc")
    {
        // create new Party & instantiate
        Party* user_party = new Party();
        user_party->members->push_back(con->id);

        // generate Party id
        std::string uuid = sole::uuid4().str();
        con->party_id = uuid;

        // store Party
        parties[uuid] = user_party;

        // protocol notation: rpa = Reply | Party | Created ~ Party ID ~ Party Name
        con->curr_packs << ":rpc~" << uuid.c_str() << "~" << user_party->party_name;
        printf("Party %s created\n", uuid.c_str());
    }
    // join Party
    else if(cmd == "pj")
    {
        printf("joining party\n");

        std::string party_id = arg.at(1);
        party_id.pop_back();

        // check if already in this party
        if(con->party_id == party_id) return;

        // remove user from previous Party (if in one)
        // TODO refactor into independent function
        if(con->party_id != "")
        {
            printf("it is not");
            auto it = parties.find(con->party_id);
            if(it != parties.end())
            {
                printf("should destroy party?\n");

                // if only one person we can erase the entire Party
                auto party = parties[con->party_id];
                if(party->members->size() == 1) parties.erase(it);
                else
                {
                    printf("did not destroy party. removed from old party.\n");

                    // find user in Party and remove them
                    auto mem_list = party->members;
                    auto it2 = std::find(mem_list->begin(), mem_list->end(), con->id);
                    if (it2 != mem_list->end()) mem_list->erase(it2);

                    // notify other members
                    for (const auto& member_id : *(party->members))
                        // protocol notation: Notice | Party | Quit
                        con_list[member_id]->curr_packs << ":npq~" << con->id;
                }

                printf("preperation complete\n");
            }
        }

        printf("id is: %s", party_id.c_str());
        // check if target party exists
        if(parties.find(party_id) != parties.end())
        {
            Party* target_party = parties[party_id];

            // add user Party
            target_party->members->push_back(con->id);

            // protocol notation: Reply | Party | Join ~ Party Name
            con->curr_packs << ":rpj~" << target_party->party_name;

            // update user Party id
            con->party_id = party_id;

            // loop through each member
            for (const auto& member_id : *(target_party->members))
            {
                // notify user that they user joined
                // protocol notation: Notice | Party | Join
                con_list[member_id]->curr_packs << ":npj~" << con->id;

                // add user to party message
                con->curr_packs << "~" << member_id;
            }

            printf("added to party\n");
        }
        else
        {
            // protocol notation: Error | Party | Join
            con->curr_packs << ":epj~party does not exist";
        }
    }
    else if(cmd == "pq")
    {
        // if only one person we can erase the entire Party
        auto party = parties[con->party_id];
        auto it = parties.find(con->party_id);
        if(party->members->size() == 1) parties.erase(it);
        else
        {
            // find user in Party and remove them
            auto mem_list = party->members;
            auto it2 = std::find(mem_list->begin(), mem_list->end(), con->id);
            if (it2 != mem_list->end()) mem_list->erase(it2);

            // notify other members
            for (const auto& member_id : *(party->members))
                // protocol notation: Notice | Party | Quit
                con_list[member_id]->curr_packs << ":npq~" << con->id;
        }

        // protocol notation: Reply | Party | Quit
        con->curr_packs << ":rpq";
    }
    // join queue
    else if(cmd == "qj")
    {
        auto it = parties.find(con->party_id);
        if(it != parties.end())
        {
            queue.push_back(con->party_id);
            con->curr_packs << ":rqj";
        }

        else con->curr_packs << ":eqj~invalid party";
    }
    // quit queue
    else if(cmd == "qq")
    {
        auto it = find (queue.begin(), queue.end(), con->party_id);
        if(it != queue.end())
        {
            queue.erase(it);
            con->curr_packs << ":rqq";
        }

        else con->curr_packs << ":eqq~not in queue";
    }
    // get packs
    else
    {
        if(con->curr_packs.str().length() > 2 || buf[0] != con->pack_state)
        {
            // store packs and flip states
            if(buf[0] == con->pack_state) {
                con->prev_packs = con->curr_packs.str();
                flip(con);
                con->curr_packs.str(std::string());
                con->curr_packs << con->pack_state;
            }

            // send packs
            if(sendto(
                ssock,
                con->prev_packs.c_str(),
                con->prev_packs.size(),
                0,
                (struct sockaddr *) client,
                *slen
            ) < 0) crash("socket sendto");
        }
    }
}

void build_match()
{
    while(true)
    {
        sleep(5);
        // sort into groups
        // TODO build match
    }
}

void protocol_handle()
{
    struct sockaddr_in si_me, si_other;

    socklen_t slen = sizeof(si_other);

    char buf[BUFLEN];

    if ((ssock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) crash("socket");

    memset((char*) &si_me, 0, sizeof(si_me));
    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(PORT);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(ssock, (struct sockaddr *)&si_me, sizeof(si_me)) == -1) crash("bind");

    while(true)
    {
        if (recvfrom(ssock, buf, BUFLEN, 0, (struct sockaddr *)&si_other, &slen) == -1) crash("recvfrom()");

        printf("Received packet from %s:%d\nData: %s\n\n", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port), buf);

        if(buf[0] == '-') check_auth(buf, &si_other);
        else if(is_set(&si_other)) process(buf, &si_other, &slen);

        memset(buf, 0, BUFLEN);
    }
}

int main(void)
{
    std::thread protocol = std::thread(protocol_handle);
    std::thread match_builder = std::thread(build_match);

    protocol.join();
    match_builder.join();

    close(ssock);
    return 0;
}
