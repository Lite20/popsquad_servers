#include "udp_server.h"


#define UDP_PORT 1153
#define BUF_SIZE 256

using std::vector;
using std::unordered_map;

int udp_sock; /* our udp socket */
ssize_t recvlen; /* # bytes received */

struct sockaddr_in server_udp_address; /* our address */
struct sockaddr_in remote_udp_address; /* remote address */
struct sockaddr_in target_udp_address; /* a pool member's address */

unsigned char udp_inbound_buffer[BUF_SIZE]; /* buffer for receiving on UDP */

unordered_map<char, vector<sockaddr_in *> *> pool_clients;
unordered_map<char, char> ip_to_pool;

vector<sockaddr_in *> * destination_pool;

char remote_ip_address[INET_ADDRSTRLEN];
char remote_ip_as_char;
char destination_pool[36]; /* contains uuid for a message's target pool */

socklen_t addrlen = sizeof(remote_udp_address);

void handle_udp()
{
    if ((udp_sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("cannot create socket\n");
        exit(1);
    }

    /* bind the socket to any valid IP address and a specific port */
    memset((char *)&server_udp_address, 0, sizeof(server_udp_address));
    server_udp_address.sin_family = AF_INET;
    server_udp_address.sin_addr.s_addr = htonl(INADDR_ANY);
    server_udp_address.sin_port = htons(UDP_PORT);
    if (bind(udp_sock, (struct sockaddr *) & server_udp_address, sizeof(server_udp_address)) < 0)
    {
        perror("udp socket bind failed\n");
        exit(1);
    }

    printf("udp waiting on port %d \n", UDP_PORT);
    while (true)
    {
        recvlen = recvfrom(udp_sock, udp_inbound_buffer, BUF_SIZE, 0, (struct sockaddr *)&remote_udp_address, &addrlen);
        if (recvlen > 0)
        {
            udp_inbound_buffer[recvlen] = 0;
            printf("udp received client message: \"%s\"\n", udp_inbound_buffer);

            const char *received_msg = reinterpret_cast<const char *>(udp_inbound_buffer);

            // extract the destination pool uuid from message
            strncpy(destination_pool, received_msg, 36);

            // store the client ip as a string (char*) in remote_ip_address
            inet_ntop(AF_INET, &(remote_udp_address.sin_addr), remote_ip_address, INET_ADDRSTRLEN);

<<<<<<< HEAD
            remote_ip_as_char = *remote_ip_address;
=======
            // cast remote ip to char type
            remote_ip_as_char = reinterpret_cast<char>(* remote_ip_address);
>>>>>>> remotes/origin/port-to-elixir

            // convert message recieved from unsigned char to const char*
            const char * recieved_msg = reinterpret_cast<const char *>(udp_inbound_buffer);

            // check if pool exists, and grab all clients in it
            if (address_pools.find(remote_ip_as_char) != address_pools.end())
            {
                // we cast destination pool to type char so we can use it as a key
                clients = pool_addresses.at(*destination_pool);
            }
            else
            {
                // make sure the client is in a pool
                if (!in_pool(remote_ip_as_char)) continue;
                send_to_pool(
                    recieved_msg,
                    ip_to_pool.at(remote_ip_as_char)
                );
            }
            else if(request == "dc")
            {
                // iterate over all the destinations
                for (auto client : clients) {
                    // get target address
                    target_udp_address = client;

                    // echo the message to other clients in the pool
                    if (sendto(udp_sock, received_msg, strlen(received_msg), 0, (struct sockaddr *)&target_udp_address, sizeof(target_udp_address)) < 0)
                    {
                        perror("udp sendto failed\n");
                        exit(1);
                    }
                }
            }
            else if(request == "sp")
            {
                // TODO add to spectate list
                send_to_pool(
                    string("sp" + remote_ip_as_char),
                    destination_pool
                );
            }
        }
    }
}

void send_to_pool(const char * msg, vector<sockaddr_in *> * dest)
{
    for(auto target_ip : &dest)
    {
        if (sendto(
            udp_sock,             // our udp socket
            msg,         // message to relay
            strlen(msg), // length of msg to relay
            0,                    // idk
            target_ip,            // target ip
            sizeof(target_ip)     // size of target ip
        ) < 0)
        {
            perror("udp sendto failed\n");
        }
    }
}

 bool in_pool(char ip_to_check)
 {
     return ip_to_pool.find(ip_to_check) == ip_to_pool.end();
 }
