#pragma once

#include <string>

std::string encrypt_payload(std::string payload, std::string inner_key, std::string outer_key);

std::string pad(std::string payload);

std::string authgen(std::string key);

std::string checksum(std::string key, std::string payload);

std::string half_day();

std::string rollkey(std::string key);
