#include <thread>

#include "tcp_server.h"
#include "udp_server.h"

int main()
{
    std::thread tcp_thread(handle_tcp);
    std::thread udp_thread(handle_udp);

    tcp_thread.join();
    udp_thread.join();

    return 0;
}
