#include "tcp_server.h"

#define TCP_PORT 1154
#define MAX_TCP_DATA_SIZE 256       /* max number of bytes we can get at once */
#define BUF_SIZE 256

struct sockaddr_in server_tcp_address;      /* our address */
struct sockaddr_in remote_tcp_address;      /* remote address */

unsigned char tcp_inbound_buffer[BUF_SIZE]; /* buffer for recieving on TCP */

using std::uint8_t;
using std::size_t;

int tcp_sock;                               /* our tcp socket */

void handle_tcp()
{
    if((tcp_sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("Cannot open tcp socket\n");
        exit(1);
    }

    memset(&server_tcp_address, 0, sizeof(server_tcp_address));

    server_tcp_address.sin_family = AF_INET;
    server_tcp_address.sin_addr.s_addr = INADDR_ANY;
    server_tcp_address.sin_port = htons(TCP_PORT);

    if(bind(tcp_sock, (struct sockaddr *)&server_tcp_address, sizeof(server_tcp_address)) < 0)
    {
        perror("Failed to bind to tcp socket\n");
        exit(1);
    }

    listen(tcp_sock, 5);
    uint len = sizeof(remote_tcp_address);
    while(true)
    {
        printf("Awaiting tcp connection\n");
        int tcp_local_socket = accept(tcp_sock, (struct sockaddr *)&remote_tcp_address, &len);
        if (tcp_local_socket < 0)
        {
            perror("Cannot accept connection\n");
            exit(0);
        }

        printf("Made TCP connection\n");
        while(true)
        {
            ssize_t socket_received_size = recv(tcp_local_socket, tcp_inbound_buffer, MAX_TCP_DATA_SIZE - 1, 0);
            if (socket_received_size < 0)
            {
                perror("Failed to receive from TCP socket\n");
                continue;
            }

            if(socket_received_size == 0)
            {
                printf("TCP Connection lost\n");
                break;
            }

            // null terminate the string
            tcp_inbound_buffer[socket_received_size] = '\0';

            printf("Received tcp server message: \"%s\"\n", tcp_inbound_buffer);

            uint8_t key[ CryptoPP::AES::DEFAULT_KEYLENGTH ], iv[ CryptoPP::AES::BLOCKSIZE ];
            memset( key, 0x00, static_cast<size_t>(CryptoPP::AES::DEFAULT_KEYLENGTH) );
            memset( iv, 0x00, static_cast<size_t>(CryptoPP::AES::BLOCKSIZE) );

            //
            // String and Sink setup
            //
            string plaintext = string(reinterpret_cast<char*>(tcp_inbound_buffer));

            //
            // Dump Plain Text
            //
            std::cout << "Plain Text (" << plaintext.size() << " bytes)" << std::endl;
            std::cout << plaintext;
            std::cout << std::endl << std::endl;

            //
            // Create Cipher Text
            //
            CryptoPP::AES::Encryption aesEncryption(key, static_cast<size_t>(CryptoPP::AES::DEFAULT_KEYLENGTH));
            CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption( aesEncryption, iv );

            string ciphertext;
            string decryptedtext;

            CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink( ciphertext ) );
            stfEncryptor.Put( reinterpret_cast<const unsigned char*>( plaintext.c_str() ), plaintext.length() + 1 );
            stfEncryptor.MessageEnd();

            //
            // Dump Cipher Text
            //
            std::cout << "Cipher Text (" << ciphertext.size() << " bytes)" << std::endl;
            for (byte i : ciphertext) {
                std::cout << std::hex << (0xFF & i);
            }

            std::cout << std::endl << std::endl;

            //
            // Decrypt
            //
            CryptoPP::AES::Decryption aesDecryption(key, static_cast<std::size_t>(CryptoPP::AES::DEFAULT_KEYLENGTH));
            CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption( aesDecryption, iv );

            CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink( decryptedtext ) );
            stfDecryptor.Put( reinterpret_cast<const unsigned char*>( ciphertext.c_str() ), ciphertext.size() );
            stfDecryptor.MessageEnd();

            //
            // Dump Decrypted Text
            //
            std::cout << "Decrypted Text: " << std::endl;
            std::cout << decryptedtext;
            std::cout << std::endl << std::endl;
        }
    }
}
