#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unordered_map>
#include <algorithm>
#include <arpa/inet.h>
#include <vector>
#include <iostream>
#include <iomanip>

#include "cryptopp/cryptlib.h"
#include "cryptopp/filters.h"
#include "cryptopp/files.h"
#include "cryptopp/modes.h"
#include "cryptopp/hex.h"
#include "cryptopp/aes.h"

void handle_udp();
